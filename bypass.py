# 
# This file is used to defined all the key that will be ignored before it's sent to the server
# 
bypassKeys = [
  # Function key
  'f1',
  'f2',
  'f3',
  'f4',
  'f5',
  'f6',
  'f7',
  'f8',
  'f9',
  'f10',
  'f11',
  'f12', 
  # Action key
  'shift',
  'ctrl',
  'alt',
  'right alt',
  'right ctrl',
  'right shift',
  # Directional key
  'right',
  'left',
  'up',
  'down',
  'home',  
  # Numpad key
  'page up',
  'page down',
  'end',
  'insert'
  'delete',
  'esc',
  'print screen',
  'left windows',
  'right windows'
]