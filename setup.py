from distutils.core import setup
import py2exe

setup(console=['main.py'])

# Change "console" to "windows" to disable the terminal
# Run this setup file using python setup.py py2exe